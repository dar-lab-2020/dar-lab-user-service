import connection from './connection';

connection.connect();

connection.query(`
      CREATE TABLE users (
          id int NOT NULL AUTO_INCREMENT,
          username varchar(255) NOT NULL,
          firstName varchar(255),
          lastName varchar(255),
          password varchar(255) NOT NULL,
          PRIMARY KEY (ID)
      )
  `, (error, result) => {
      if (error) throw error;
      console.log('The result is: ', result);
  });

connection.end();
import connection from './connection';

connection.connect();

connection.query(`
      CREATE TABLE tokens (
          id int NOT NULL AUTO_INCREMENT,
          token varchar(255) NOT NULL,
          user_id int NOT NULL,
          PRIMARY KEY (ID),
          FOREIGN KEY(user_id) REFERENCES users(id)
      )
  `, (error, result) => {
      if (error) throw error;
      console.log('The result is: ', result);
  });

connection.end();
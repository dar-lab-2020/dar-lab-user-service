import express from 'express';
import UserController from './controllers/users.controller';

const app = express();

app.set('port', process.env.PORT || 3000);

app.use(express.json());

app.get('/', (req, res) => {
    res.send('Hello world');
});

app.use('/users', (new  UserController()).initRoutes());

export default app;